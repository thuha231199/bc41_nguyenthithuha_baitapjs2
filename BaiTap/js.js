/**Bài 1: Tiền lương nhân viên
 * - Đầu vào: 
 * Lương 1 ngày: 100.000
 * Nhập số ngày làm
 * - Đầu ra:
 * Tiên lương của nhân viên = số ngày làm x 100000
 */
 var soNgayLamEl = document.getElementById ("soNgayLam")
 var tienLuongEl = document.getElementById ("tienLuong")
 function tinhTien() {
    console.log(tienLuongEl.value = soNgayLamEl.value*100000)
 }
 /** Bài 2: Tính giá trị trung bình
  * - Đầu vào:
  * Nhập 5 số thực
  * num1 =....
  * num2 = ....
  * num3 = ....
  * num4 = ....
  * num5 =....
  * - Đầu ra 
  * Trung bình = (num1 + num2 + num3 + num4 + num5) /5
  */
  function sum(){
    let a = Number(document.getElementById("num1").value);
    let b = Number(document.getElementById("num2").value);
    let c = Number(document.getElementById("num3").value);
    let d = Number(document.getElementById("num4").value);
    let e = Number(document.getElementById("num5").value);
    let sum = (parseInt(a) + b + c + d + e)/5;

    document.getElementById('result').innerHTML = sum;
}
 /**Bài 3: Quy đổi tiền
  * - Đầu ra
  * Gán thẻ usa
  * Gán thẻ vnd
  * - Đầu ra
  * vnd = usd * 23500.
  */
 var usaEl = document.getElementById("usa");
 var vndEl= document.getElementById("vnd");
 function quyDoi(){
     console.log(vndEl.value=usaEl.value *23500)
 }
 /**Bài 4: Tính chu vi, diện tích HCN
  * - Đầu vào: Nhập số đo
  * Chiều dài :...
  * Chiều rộng:...
  * - Đầu ra:
  * Diện tích:
  * Chu vi: 
  */
  function chuVi(){
    let e = Number(document.getElementById("chieuDai").value);
    let f = Number(document.getElementById("chieuRong").value);
    let chuVi = (parseInt(e) + f ) * 2;
    document.getElementById('result_1').innerHTML = chuVi;
}
function dienTich(){
    let e = Number(document.getElementById("chieuDai").value);
    let f = Number(document.getElementById("chieuRong").value);
    let dienTich = (parseInt(e) * f );
    document.getElementById('result').innerHTML = dienTich;
}
 /**Bài 5: Tính tổng 2 kí số vừa nhập
  * - Đầu vào
  * Gán giá trị 
  * Gán kết quả
  * - Đầu ra 
  * Kết quả = giá trị gán chia cho 10 lấy dư + giá trị gán chia cho 10 làm tròn xuống 
  */
  var numberEl = document.getElementById ("number")
  var ketQuaEl = document.getElementById ("ketQua")
  function tong() {
     console.log(ketQuaEl.value = (numberEl.value%10) + Math.floor(numberEl.value/10))
  }
 
